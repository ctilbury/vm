#ifndef TREES_H
#define TREES_H

#include <cstring>

template <typename T> class Tree {

    struct element {
        const char* key;
        T data;
        struct element* left;
        struct element* right;
    };

    struct element* root;

    const char* string_dup(const char* str) {
        size_t len = strlen(str) + 1;
        const char* tmp = new char[len];
        memcpy((void*)tmp, (void*)str, len);
        return tmp;
    }

    void add_node(struct element* tree, struct element* node) {
        int x = strcmp(tree->key, node->key);
        if(x > 0) {
            if(tree->right != nullptr)
                add_node(tree->right, node);
            else
                tree->right = node;
        }
        else if(x < 0) {
            if(tree->left != nullptr)
                add_node(tree->left, node);
            else
                tree->left = node;
        }
        else { // replace existing data
            tree->data = node->data;
            delete node->key;
            delete node;
        }
    }

    struct element* find_node(const char* key, struct element* tree) {
        int x = strcmp(tree->key, key); //strcmp(tree->key, key);
        if(x > 0) {
            if(tree->right != nullptr)
                return find_node(key, tree->right);
            else
                return nullptr;
        }
        else if(x < 0) {
            if(tree->left != nullptr)
                return find_node(key, tree->left);
            else
                return nullptr;
        }
        else
            return tree;
    }

    bool del_node(const char* key, struct element* node) {
        if(node->right != nullptr) {
            int x = strcmp(key, node->right->key);
            if(x > 0)
                return del_node(key, node->right);
            else if(x < 0)
                return del_node(key, node->left);
            else {
                struct element* tmp = node->right;
                node->right = nullptr;

                if(tmp->left != nullptr)
                    add_node(root, tmp->left);
                if(tmp->right != nullptr)
                    add_node(root, tmp->right);

                delete tmp->key;
                delete tmp;
                return true;
            }
        }

        if(node->left != nullptr) {
            int x = strcmp(key, node->left->key);
            if(x > 0)
                return del_node(key, node->right);
            else if(x < 0)
                return del_node(key, node->left);
            else {
                struct element* tmp = node->left;
                node->left = nullptr;

                if(tmp->left != nullptr)
                    add_node(root, tmp->left);
                if(tmp->right != nullptr)
                    add_node(root, tmp->right);

                delete tmp->key;
                delete tmp;
                return true;
            }
        }

        return false;
    }

    void kill_tree(struct element* node) {
        if(node->left != nullptr)
            kill_tree(node->left);
        if(node->right != nullptr)
            kill_tree(node->right);

        delete node;
    }

    void dump_nodes(struct element* node) {
        if(node->left != nullptr)
            dump_nodes(node->left);
        if(node->right != nullptr)
            dump_nodes(node->right);

        std::cout << node->key << " = " << node->data;
        std::cout << " node: " << node;
        std::cout << " left: " << node->left;
        std::cout << " right: " << node->right;
        std::cout << std::endl;
    }

    // public interface
    public:

    Tree() { root = nullptr; }

    Tree(const char* key, T data) {
        struct element* node = new struct element;
        node->key = string_dup(key);
        node->data = data;

        node->left = nullptr;
        node->right = nullptr;

        root = node;
    }

    ~Tree() {
        kill_tree(root);
        //std::cout << "destructor\n";
    }

    void add(const char* key, T data) {
        struct element* node = new struct element;
        node->key = string_dup(key);
        node->data = data;

        node->left = nullptr;
        node->right = nullptr;

        if(root != nullptr)
            add_node(root, node);
        else
            root = node;
    }

    T find(const char* key) {
        struct element* tmp = find_node(key, root);
        if(tmp != nullptr)
            return tmp->data;
        else
            throw "key not found";
    }

    bool del(const char* key) {
        if(!strcmp(key, root->key)) {
            struct element* tmp = root;
            if(tmp->left != nullptr) {
                root = tmp->left;
                if(tmp->right != nullptr)
                    add_node(root, tmp->right);
            }
            else if(tmp->right != nullptr) {
                root = tmp->right;
                if(tmp->right != nullptr)
                    add_node(root, tmp->right);
            }
            else
                root = nullptr;

            delete tmp->key;
            delete tmp;
            return true;
        }
        else
            return del_node(key, root);
    }

    void dump() {
        dump_nodes(root);
    }
};

#endif

