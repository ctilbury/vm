
#ifndef LOGGER_H
#define LOGGER_H

#include <cstdio>
#include <cstdarg>
#include <ctime>

using namespace std;

enum class LogType {
    DEBUG,
    INFO ,
    WARN ,
    ERROR,
};

class Logger {

    bool opened = false;
    LogType msglevel = LogType::DEBUG;
    FILE* outhandle;

    void write(LogType type, const char* fmt, ...) {
        if(type >= msglevel) {
            time_t tm = time(nullptr);
            char buff[50];
            strftime(buff, sizeof(buff), "%y-%m-%d %OH:%OM:%OS", localtime(&tm));
            fprintf(outhandle, "%s [%s] ", buff, getLabel(type));
                    // std::put_time(std::localtime(&tm), "%y-%m-%d %OH:%OM:%OS"),
                    // getLabel(type));
            va_list args;
            va_start(args, fmt);
            vfprintf(outhandle, fmt, args);
            va_end(args);
            fprintf(outhandle, "\n");
        }
    }

    const char* getLabel(LogType type) {
        return (type == LogType::DEBUG)? "DEBUG":
            (type == LogType::INFO )? "INFO ":
            (type == LogType::WARN )? "WARN ":
            (type == LogType::ERROR)? "ERROR": "UNKNOWN";
    }

public:
    Logger() {
        msglevel = LogType::ERROR;
        outhandle = stderr;
    }

    Logger(LogType type) {
        msglevel = type;
        outhandle = stderr;
    }

    Logger(FILE* fp) {
        outhandle = fp;
    }

    Logger(const char* fname, LogType type) {
        msglevel = type;
        outhandle = fopen(fname, "a");
    }

    void info(const char* fmt, ...) {
        va_list args;
        va_start(args, fmt);
        write(LogType::INFO, fmt, args);
        va_end(args);
    }

    void debug(const char* fmt, ...) {
        va_list args;
        va_start(args, fmt);
        write(LogType::DEBUG, fmt, args);
        va_end(args);
    }

    void warn(const char* fmt, ...) {
        va_list args;
        va_start(args, fmt);
        write(LogType::WARN, fmt, args);
        va_end(args);
    }

    void error(const char* fmt, ...) {
        va_list args;
        va_start(args, fmt);
        write(LogType::ERROR, fmt, args);
        va_end(args);
    }


};

#endif  /* LOGGER_H */
