#ifndef VARIABLES_H
#define VARIABLES_H

enum class VarType {
    ERROR,
    NONE,
    INUM,
    UNUM,
    FNUM,
    STRG,
    BOOL,
    DICT,
    LIST,
    ADDR,
    UTYPE,
};

#include "lists.h"
#include "errors.h"
#include <cstdint>
#include <string>

class NativeDict {};
class NativeList {};
class Value {

    VarType type;
    bool isAssigned;
    bool isConst;
    bool isLiteral;
    bool isDeleted;
    Error* err;

    union {
		uint32_t unum;
		int32_t inum;
		float fnum;
		const char* str;
        bool boolean;
		uint32_t addr;
		void* utype;
        NativeDict dict;
        NativeList list;
	} data;

    // public interface
    public:

    Value();
    Value(VarType t);
    Value(Value& v);
    Value(uint32_t val);
    Value(int32_t val);
    Value(float val);
    Value(bool val);
    Value(const char* val);
    Value(char* val);
    Value(std::string val);
    Value(void* val);
    Value(NativeDict& val);
    Value(NativeList& val);

    uint32_t getUnum();
    int32_t getInum();
    float getFnum();
    bool getBool();
    const char* getStr();
    uint32_t getAddr();
    void* getUtype();
    NativeDict& getDict();
    NativeList& getList();

    void set(uint32_t val);
    void set(int32_t val);
    void set(float val);
    void set(bool val);
    void set(const char* val);
    void set(char* val);
    void set(std::string val);
    void set(void* val);
    void set(NativeDict& val);
    void set(NativeList& val);

    // arithmetic operators
    Value& operator=(uint32_t val);
    Value& operator=(int32_t val);
    Value& operator=(float val);
    Value& operator=(bool val);
    Value& operator=(const char* val);
    Value& operator=(char* val);
    Value& operator=(std::string val);
    Value& operator=(void* val);
    Value& operator=(NativeDict& val);
    Value& operator=(NativeList& val);
    Value& operator=(Value& val);

    Value& operator+(uint32_t val);
    Value& operator+(int32_t val);
    Value& operator+(float val);
    Value& operator+(bool val);
    Value& operator+(const char* val);
    Value& operator+(char* val);
    Value& operator+(std::string val);
    Value& operator+(void* val);
    Value& operator+(NativeDict& val);
    Value& operator+(NativeList& val);
    Value& operator+(Value& val);

    Value& operator-(uint32_t val);
    Value& operator-(int32_t val);
    Value& operator-(float val);
    Value& operator-(bool val);
    Value& operator-(const char* val);
    Value& operator-(char* val);
    Value& operator-(std::string val);
    Value& operator-(void* val);
    Value& operator-(NativeDict& val);
    Value& operator-(NativeList& val);
    Value& operator-(Value& val);
    Value& operator-(); // unary

    Value& operator*(uint32_t val);
    Value& operator*(int32_t val);
    Value& operator*(float val);
    Value& operator*(bool val);
    Value& operator*(const char* val);
    Value& operator*(char* val);
    Value& operator*(std::string val);
    Value& operator*(void* val);
    Value& operator*(NativeDict& val);
    Value& operator*(NativeList& val);
    Value& operator*(Value& val);

    Value& operator/(uint32_t val);
    Value& operator/(int32_t val);
    Value& operator/(float val);
    Value& operator/(bool val);
    Value& operator/(const char* val);
    Value& operator/(char* val);
    Value& operator/(std::string val);
    Value& operator/(void* val);
    Value& operator/(NativeDict& val);
    Value& operator/(NativeList& val);
    Value& operator/(Value& val);

    Value& operator%(uint32_t val);
    Value& operator%(int32_t val);
    Value& operator%(float val);
    Value& operator%(bool val);
    Value& operator%(const char* val);
    Value& operator%(char* val);
    Value& operator%(std::string val);
    Value& operator%(void* val);
    Value& operator%(NativeDict& val);
    Value& operator%(NativeList& val);
    Value& operator%(Value& val);

    // comparison operators
    Value& operator==(uint32_t val);
    Value& operator==(int32_t val);
    Value& operator==(float val);
    Value& operator==(bool val);
    Value& operator==(const char* val);
    Value& operator==(char* val);
    Value& operator==(std::string val);
    Value& operator==(void* val);
    Value& operator==(NativeDict& val);
    Value& operator==(NativeList& val);
    Value& operator==(Value& val);

    Value& operator!=(uint32_t val);
    Value& operator!=(int32_t val);
    Value& operator!=(float val);
    Value& operator!=(bool val);
    Value& operator!=(const char* val);
    Value& operator!=(char* val);
    Value& operator!=(std::string val);
    Value& operator!=(void* val);
    Value& operator!=(NativeDict& val);
    Value& operator!=(NativeList& val);
    Value& operator!=(Value& val);

    Value& operator<(uint32_t val);
    Value& operator<(int32_t val);
    Value& operator<(float val);
    Value& operator<(bool val);
    Value& operator<(const char* val);
    Value& operator<(char* val);
    Value& operator<(std::string val);
    Value& operator<(void* val);
    Value& operator<(NativeDict& val);
    Value& operator<(NativeList& val);
    Value& operator<(Value& val);

    Value& operator>(uint32_t val);
    Value& operator>(int32_t val);
    Value& operator>(float val);
    Value& operator>(bool val);
    Value& operator>(const char* val);
    Value& operator>(char* val);
    Value& operator>(std::string val);
    Value& operator>(void* val);
    Value& operator>(NativeDict& val);
    Value& operator>(NativeList& val);
    Value& operator>(Value& val);

    Value& operator<=(uint32_t val);
    Value& operator<=(int32_t val);
    Value& operator<=(float val);
    Value& operator<=(bool val);
    Value& operator<=(const char* val);
    Value& operator<=(char* val);
    Value& operator<=(std::string val);
    Value& operator<=(void* val);
    Value& operator<=(NativeDict& val);
    Value& operator<=(NativeList& val);
    Value& operator<=(Value& val);

    Value& operator>=(uint32_t val);
    Value& operator>=(int32_t val);
    Value& operator>=(float val);
    Value& operator>=(bool val);
    Value& operator>=(const char* val);
    Value& operator>=(char* val);
    Value& operator>=(std::string val);
    Value& operator>=(void* val);
    Value& operator>=(NativeDict& val);
    Value& operator>=(NativeList& val);
    Value& operator>=(Value& val);

    Value& operator!();

    void load(FILE* fp);
    void save(FILE* fp);
    void print();

    const char* varTypeToStr(VarType type) {
        return (type == VarType::ERROR)? "ERROR" :
                (type == VarType::NONE)? "NONE" :
                (type == VarType::INUM)? "INUM" :
                (type == VarType::UNUM)? "UNUM" :
                (type == VarType::FNUM)? "FNUM" :
                (type == VarType::STRG)? "STRG" :
                (type == VarType::BOOL)? "BOOL" :
                (type == VarType::DICT)? "DICT" :
                (type == VarType::LIST)? "LIST" :
                (type == VarType::ADDR)? "ADDR" :
                (type == VarType::UTYPE)? "UTYPE" : "UNKNOWN";
    }
};

class VarStore {

    List<Value> store;

public:
    VarStore();

    void save(FILE* fp);
    void load(FILE* fp);

    void add(Value var);
    Value get(Size index);
    void del(Size index);
};


#endif
