#ifndef OPCODES_H
#define OPCODES_H

enum class Op {
    ERROR,   // payload is a string with the error, set by the VM
    BREAK,   // Cause the debugger to be entered
    NOOP,    // no operation
    EXIT,    // cause the VM to exit
    SAVE,    // save the top of the stack to the slot number (constant)
    LOCAL,   // save top of stack to a local

    CAST,    // change the type of a variable at a slot number
    PRINTS,  // print the value at the top of the stack
    PRINT,   // print the value given by the index operand

    TRAP,    // (N.I.) immediate is an 8 bit trap number
    EXCEPT,  // (N.I.) raise an exception and process an exception handler (constatant)
    CALLX,   // (N.I.) call an externally defined function (constatant)

    CALL,    // call an absolute address (constatant)
    RETURN,  // return from a call
    JMP,     // jump to an absolute address (constatant)
    JMPIF,   // conditional jump to an absolute address (constatant)

    PUSH,    // push the value slot on the value stack (variable)
    PUSH8,   // push an immediate with type info.
    PUSH16,
    PUSH32,
    POP,     // pop the value from the stack and throw it away
    PEEK,    // look at the value stack without popping

    // comparison operators
    NOT,     // unary not conditional
    EQ,      // equal conditional
    NEQ,     // not equal conditional
    LEQ,     // less-or-equal conditional
    GEQ,     // greater-or-equal conditional
    LESS,    // less than conditional
    GTR,     // greater than conditional

    // arithmetic operators
    NEG,     // unary arithmetic negation
    ADD,     // arithmetic add
    SUB,     // arithmetic subtract
    MUL,     // arithmetic multiply
    DIV,     // arithmetic divide
    MOD,     // arithmetic modulo
};

#include <cstdio>
#include <cstdint>
#include "lists.h"

class OpCodes {

    List<uint8_t> ops;
    Size index;

    // public interface
    public:

    OpCodes() {
        index = 0;
    }

    uint8_t read_u8() {
        return ops.get(index++);
    }

    uint16_t read_u16() {
        uint16_t word = 0;
        word = ops.get(index++);
        word |= ops.get(index++) << 8;
        return word;
    }

    uint32_t read_u32() {
        uint32_t word = 0;
        word = ops.get(index++);
        word |= ops.get(index++) << 8;
        word |= ops.get(index++) << 16;
        word |= ops.get(index++) << 24;
        return word;
    }

    int8_t read_i8() { return int8_t(read_u8()); }
    int16_t read_i16() { return int16_t(read_u16()); }
    int32_t read_i32() { return int32_t(read_u32()); }

    void write_u8(uint8_t word) {
        ops.add(word);
    }

    void write_u16(uint16_t word) {
        ops.add(word & 0xFF);
        ops.add((word >> 8) & 0xFF);
    }

    void write_u32(uint32_t word) {
        ops.add(word & 0xFF);
        ops.add((word >> 8) & 0xFF);
        ops.add((word >> 16) & 0xFF);
        ops.add((word >> 24) & 0xFF);
    }

    void write_i8(int8_t word) { write_u8(uint8_t(word)); }
    void write_i16(int16_t word) { write_u16(uint16_t(word)); }
    void write_i32(int32_t word) { write_u32(uint32_t(word)); }

    uint32_t getIndex() { return index; }
    void setIndex(uint32_t idx) { index = idx; }

    void load(FILE* fp) {
        ops.load(fp);
        index = 0;
    }

    void save(FILE* fp) {
        ops.save(fp);
    }

    const char* opCodeToStr(Op code) {
        return (code == Op::ERROR)? "ERROR" :
                (code == Op::BREAK)? "BREAK" :
                (code == Op::NOOP)? "NOOP" :
                (code == Op::EXIT)? "EXIT" :
                (code == Op::SAVE)? "SAVE" :
                (code == Op::LOCAL)? "LOCAL" :
                (code == Op::CAST)? "CAST" :
                (code == Op::PRINTS)? "PRINTS" :
                (code == Op::PRINT)? "PRINT" :
                (code == Op::TRAP)? "TRAP" :
                (code == Op::EXCEPT)? "EXCEPT" :
                (code == Op::CALLX)? "CALLX" :
                (code == Op::CALL)? "CALL" :
                (code == Op::RETURN)? "RETURN" :
                (code == Op::JMP)? "JMP" :
                (code == Op::JMPIF)? "JMPIF" :
                (code == Op::PUSH)? "PUSH" :
                (code == Op::PUSH8)? "PUSH8" :
                (code == Op::PUSH16)? "PUSH16" :
                (code == Op::PUSH32)? "PUSH32" :
                (code == Op::POP)? "POP" :
                (code == Op::PEEK)? "PEEK" :
                (code == Op::NOT)? "NOT" :
                (code == Op::EQ)? "EQ" :
                (code == Op::NEQ)? "NEQ" :
                (code == Op::LEQ)? "LEQ" :
                (code == Op::GEQ)? "GEQ" :
                (code == Op::LESS)? "LESS" :
                (code == Op::GTR)? "GTR" :
                (code == Op::NEG)? "NEG" :
                (code == Op::ADD)? "ADD" :
                (code == Op::SUB)? "SUB" :
                (code == Op::MUL)? "MUL" :
                (code == Op::DIV)? "DIV" :
                (code == Op::MOD)? "MOD" : "UNKNOWN";
    }

    void dump() { ops.dump(); }

};

#endif