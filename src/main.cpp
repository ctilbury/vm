#include <bits/stdc++.h>
#include <cstdio>

#include "lists.h"
#include "trees.h"
#include "logger.h"
#include "opcodes.h"
#include "errors.h"

int main() {

    Logger logger = Logger(LogType::INFO);
    Error* err = Error::getInstance();

    err->error("this is a %d error", 123);

    logger.info("add some strs to a list");
    List<const char*> strs;
    strs.add("plather glop");
    strs.add("slop plop");
    strs.add("nonedeloop");
    strs.add("bacon with eggs");
    strs.dump();
    logger.info("dump the strs\n");

    logger.info("add some ints to a list");
    List<int> ints;
    ints.add(123);
    ints.add(8086);
    ints.add(876);
    ints.add(1);
    ints.add(500203947);
    ints.dump();
    logger.info("dump the list of ints\n");

    logger.info("testing trees");
    Tree<std::string> s;
    s.add("asd", "239479");
    s.add("sdf", "234234234");
    s.add("wer", "123234343");
    s.add("ggh", "56756776");
    s.add("ytuyu", "45645");
    s.add("tyu", "345345");
    s.add("045", "98934");
    s.add("lkhj", "9397563553");
    s.add("iert", "932497");
    s.dump();
    logger.info("dump list\n");


    logger.info("find: %s = %s", "045", s.find("045"));
    logger.info("find: %s = %s", "sdf", s.find("sdf"));
    logger.info("find: %s = %s", "asd", s.find("asd"));

    logger.info("delete some entries");
    s.del("045");
    s.del("asd");
    s.del("lkhj");
    s.dump();
    logger.info("dump the entries\n");

    logger.info("find invalid entry\n");
    try {
        logger.info("find: %s", s.find("asd"));
    }
    catch(const char* e) {
        logger.info(e);
    }

    OpCodes ops;
    ops.write_u8(0x13);
    printf("read code: 0x%X\n", ops.read_u8());
    ops.write_u32(0x18805);
    printf("read code: 0x%X\n", ops.read_u32());
    ops.setIndex(0);
    printf("read code: 0x%X\n", ops.read_u8());
    printf("read code: 0x%X\n", ops.read_u32());

    ops.write_i8(-13);
    ops.write_i16(-72);
    printf("read code: %d\n", ops.read_i8());
    printf("read code: %d\n", ops.read_i16());

    FILE* fp = fopen("test.file", "w");
    ops.save(fp);
    fclose(fp);

    fp = fopen("test.file", "r");
    OpCodes ops1;
    ops1.load(fp);
    fclose(fp);
    printf("read code: 0x%X\n", ops1.read_u8());
    printf("read code: 0x%X\n", ops1.read_u32());
    printf("read code: %d\n", ops1.read_i8());
    printf("read code: %d\n", ops1.read_i16());

}

