
#include "variables.h"

Value::Value() {
    type = VarType::NONE;
    isAssigned = false;
    err = Error::getInstance();
}

Value::Value(VarType t) {
    type = t;
    isAssigned = false;
    err = Error::getInstance();
}

Value::Value(Value& v) {
    type = v.type;
    isAssigned = v.isAssigned;
    isConst = v.isConst;
    isLiteral = v.isLiteral;
    isDeleted = v.isDeleted;
    data = v.data;
    err = Error::getInstance();
}

Value::Value(uint32_t val) {
    type = VarType::UNUM;
    data.unum = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(int32_t val) {
    type = VarType::INUM;
    data.inum = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(float val) {
    type = VarType::FNUM;
    data.fnum = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(bool val) {
    type = VarType::BOOL;
    data.boolean = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(const char* val) {
    type = VarType::STRG;
    data.str = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(char* val) {
    type = VarType::STRG;
    data.str = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(std::string val) {
    type = VarType::STRG;
    data.str = val.c_str();
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(void* val) {
    type = VarType::UTYPE;
    data.utype = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(NativeDict& val) {
    type = VarType::DICT;
    data.dict = val;
    isAssigned = true;
    err = Error::getInstance();
}

Value::Value(NativeList& val) {
    type = VarType::LIST;
    data.list = val;
    isAssigned = true;
    err = Error::getInstance();
}

uint32_t Value::getUnum() {
    switch(type) {
        case VarType::INUM:
            return (uint32_t)data.inum;
        case VarType::UNUM:
            return data.unum;
        case VarType::FNUM:
            return (uint32_t)((int32_t)data.fnum);
        case VarType::STRG:
            err->warning("convert STRG to UNUM is not supported yet");
            break;
        case VarType::ADDR:
            return data.addr;
        case VarType::ERROR:
        case VarType::NONE:
        case VarType::BOOL:
        case VarType::DICT:
        case VarType::LIST:
        case VarType::UTYPE:
            err->error("cannot convert a %s type to a UNUM.", varTypeToStr(type));
            break;
        default:
            err->fatal("unknown type in getUnum(): %d", type);
    }
    return 0;
}

int32_t Value::getInum() { return data.inum; }
float Value::getFnum() { return data.fnum; }
bool Value::getBool() { return data.boolean; }
const char* Value::getStr() { return data.str; }
uint32_t Value::getAddr() { return data.addr; }
void* Value::getUtype() { return data.utype; }
NativeDict& Value::getDict() { return data.dict; }
NativeList& Value::getList() { return data.list; }

void Value::set(uint32_t val) { data.unum = val; }
void Value::set(int32_t val) { data.inum = val; }
void Value::set(float val) { data.fnum = val; }
void Value::set(bool val) { data.boolean = val; }
void Value::set(const char* val) { data.str = val; }
void Value::set(char* val) { data.str = val; }
void Value::set(std::string val) { data.str = val.c_str(); }
void Value::set(void* val) { data.utype = val; }
void Value::set(NativeDict& val) { data.dict = val; }
void Value::set(NativeList& val) { data.list = val; }

// arithmetic operators
Value& Value::operator=(uint32_t val) {}
Value& Value::operator=(int32_t val) {}
Value& Value::operator=(float val) {}
Value& Value::operator=(const char* val) {}
Value& Value::operator=(char* val) {}
Value& Value::operator=(std::string val) {}
Value& Value::operator=(void* val) {}
Value& Value::operator=(NativeDict& val) {}
Value& Value::operator=(NativeList& val) {}
Value& Value::operator=(Value& val) {}

Value& Value::operator+(uint32_t val) {}
Value& Value::operator+(int32_t val) {}
Value& Value::operator+(float val) {}
Value& Value::operator+(const char* val) {}
Value& Value::operator+(char* val) {}
Value& Value::operator+(std::string val) {}
Value& Value::operator+(void* val) {}
Value& Value::operator+(NativeDict& val) {}
Value& Value::operator+(NativeList& val) {}
Value& Value::operator+(Value& val) {}

Value& Value::operator-(uint32_t val) {}
Value& Value::operator-(int32_t val){}
Value& Value::operator-(float val){}
Value& Value::operator-(const char* val){}
Value& Value::operator-(char* val){}
Value& Value::operator-(std::string val){}
Value& Value::operator-(void* val){}
Value& Value::operator-(NativeDict& val){}
Value& Value::operator-(NativeList& val){}
Value& Value::operator-(Value& val){}
Value& Value::operator-() {}

Value& Value::operator*(uint32_t val) {}
Value& Value::operator*(int32_t val) {}
Value& Value::operator*(float val) {}
Value& Value::operator*(const char* val) {}
Value& Value::operator*(char* val) {}
Value& Value::operator*(std::string val) {}
Value& Value::operator*(void* val) {}
Value& Value::operator*(NativeDict& val) {}
Value& Value::operator*(NativeList& val) {}
Value& Value::operator*(Value& val) {}

Value& Value::operator/(uint32_t val) {}
Value& Value::operator/(int32_t val) {}
Value& Value::operator/(float val) {}
Value& Value::operator/(const char* val) {}
Value& Value::operator/(char* val) {}
Value& Value::operator/(std::string val) {}
Value& Value::operator/(void* val) {}
Value& Value::operator/(NativeDict& val) {}
Value& Value::operator/(NativeList& val) {}
Value& Value::operator/(Value& val) {}

Value& Value::operator%(uint32_t val) {}
Value& Value::operator%(int32_t val) {}
Value& Value::operator%(float val) {}
Value& Value::operator%(const char* val) {}
Value& Value::operator%(char* val) {}
Value& Value::operator%(std::string val) {}
Value& Value::operator%(void* val) {}
Value& Value::operator%(NativeDict& val) {}
Value& Value::operator%(NativeList& val) {}
Value& Value::operator%(Value& val) {}

// comparison operators
Value& Value::operator==(uint32_t val) {}
Value& Value::operator==(int32_t val) {}
Value& Value::operator==(float val) {}
Value& Value::operator==(const char* val) {}
Value& Value::operator==(char* val) {}
Value& Value::operator==(std::string val) {}
Value& Value::operator==(void* val) {}
Value& Value::operator==(NativeDict& val) {}
Value& Value::operator==(NativeList& val) {}
Value& Value::operator==(Value& val) {}

Value& Value::operator!=(uint32_t val) {}
Value& Value::operator!=(int32_t val) {}
Value& Value::operator!=(float val) {}
Value& Value::operator!=(const char* val) {}
Value& Value::operator!=(char* val) {}
Value& Value::operator!=(std::string val) {}
Value& Value::operator!=(void* val) {}
Value& Value::operator!=(NativeDict& val) {}
Value& Value::operator!=(NativeList& val) {}
Value& Value::operator!=(Value& val) {}

Value& Value::operator<(uint32_t val) {}
Value& Value::operator<(int32_t val) {}
Value& Value::operator<(float val) {}
Value& Value::operator<(const char* val) {}
Value& Value::operator<(char* val) {}
Value& Value::operator<(std::string val) {}
Value& Value::operator<(void* val) {}
Value& Value::operator<(NativeDict& val) {}
Value& Value::operator<(NativeList& val) {}
Value& Value::operator<(Value& val) {}

Value& Value::operator>(uint32_t val) {}
Value& Value::operator>(int32_t val) {}
Value& Value::operator>(float val) {}
Value& Value::operator>(const char* val) {}
Value& Value::operator>(char* val) {}
Value& Value::operator>(std::string val) {}
Value& Value::operator>(void* val) {}
Value& Value::operator>(NativeDict& val) {}
Value& Value::operator>(NativeList& val) {}
Value& Value::operator>(Value& val) {}

Value& Value::operator<=(uint32_t val) {}
Value& Value::operator<=(int32_t val) {}
Value& Value::operator<=(float val) {}
Value& Value::operator<=(const char* val) {}
Value& Value::operator<=(char* val) {}
Value& Value::operator<=(std::string val) {}
Value& Value::operator<=(void* val) {}
Value& Value::operator<=(NativeDict& val) {}
Value& Value::operator<=(NativeList& val) {}
Value& Value::operator<=(Value& val) {}

Value& Value::operator>=(uint32_t val) {}
Value& Value::operator>=(int32_t val) {}
Value& Value::operator>=(float val) {}
Value& Value::operator>=(const char* val) {}
Value& Value::operator>=(char* val) {}
Value& Value::operator>=(std::string val) {}
Value& Value::operator>=(void* val) {}
Value& Value::operator>=(NativeDict& val) {}
Value& Value::operator>=(NativeList& val) {}
Value& Value::operator>=(Value& val) {}

Value& Value::operator!() {}

void Value::load(FILE* fp) {}
void Value::save(FILE* fp) {}
void Value::print() {}

VarStore::VarStore() {}

void VarStore::save(FILE* fp) {}
void VarStore::load(FILE* fp) {}

void VarStore::add(Value var) {}
Value VarStore::get(Size index) {}
void VarStore::del(Size index) {}
