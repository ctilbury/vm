/**
 * @file lists.h
 * @brief Simple generic list template.
 */
#ifndef LISTS_H
#define LISTS_H

#include <cstdio>

typedef unsigned long Size;

/**
 * @brief Simple list class to store generic pointers in a list.
 */
template <typename T> class List {

    T* list;
    Size cap;
    Size len;

    // public interface
    public:

    List() {
        cap = 0x01 << 3;
        len = 0;
        list = new T[cap];
        std::cout << "list constructor\n";
    }

    Size add(T data) {
        if(len+1 >= cap) {
            cap <<= 1;
            T* tmp = new T[cap];

            for(Size i = 0; i < len; i++)
                tmp[i] = list[i];

            delete[] list;
            list = tmp;
        }

        list[len] = data;

        len++;
        return len-1;
    }

     Size push(T data) {
        return add(data);
    }

    T get(Size idx) {
        if(idx < len) // unsigned
            return list[idx];
        else
            return T(0);
    }

    T pop() {
        if(len >= 1) // unsigned
            len--;

        return list[len]; // caller is responsible for free()
    }

    T peek() {
        return get(len-1);
    }

    Size capacity() { return cap; }
    Size length() { return len; }

    void load(FILE* fp) {
        fread(&len, sizeof(len), 1, fp);

        cap = 1;
        while(cap <= len)
            cap <<= 1;

        list = new T[cap];
        fread(list, sizeof(T), len, fp);
    }

    void save(FILE* fp) {
        fwrite(&len, sizeof(len), 1, fp);
        fwrite(list, sizeof(T), len, fp);
    }

    void dump() {
        for(int i = 0; i < len; i++)
            std::cout << list[i] << std::endl;
    }
};

#endif

