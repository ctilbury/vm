#ifndef ERRORS_H
#define ERRORS_H

#include <iostream>
#include <cstdarg>

class Error {

    // private constructor and destructor
    Error() {
        //std::cout << "Error instance created!\n";
        Error::errors = 0;
        Error::warnings = 0;
    }
    ~Error() {}


    // private copy constructor and assignment operator
    Error(const Error&);
    Error& operator=(const Error&);

    static Error *m_instance;
    unsigned int errors;
    unsigned int warnings;

    // public interface
    public:

    static Error* getInstance() {
        return (!m_instance) ?
            m_instance = new Error :
            m_instance;
    }

    void error(const char* fmt, ...) {
        fprintf(stderr, "Error: ");
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
        fprintf(stderr, "\n");
        m_instance->errors++;
    }

    void warning(const char* fmt, ...) {
        fprintf(stderr, "Warning: ");
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
        fprintf(stderr, "\n");
        m_instance->warnings++;
    }

    void fatal(const char* fmt, ...) {
        fprintf(stderr, "Error: ");
        va_list args;
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
        fprintf(stderr, "\n");
        exit(1);
    }

    unsigned int getErrors() {
        return m_instance->errors;
    }

    unsigned int getWarnings() {
        return m_instance->warnings;
    }
};

//Error* Error::m_instance = nullptr;

#endif

